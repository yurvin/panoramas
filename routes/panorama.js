var express = require('express');
var router = express.Router();

router.get('/', (req, res, next) => {
    res.render('panorama', {title: 'panorama'})
});

module.exports = router;
