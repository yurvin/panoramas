var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var mapsRouter = require('./routes/maps');
var panoramaRouter = require('./routes/panorama');
var fullRouter = require('./routes/full');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/maps', mapsRouter);
app.use('/panorama', panoramaRouter);
app.use('/full', fullRouter);

module.exports = app;
