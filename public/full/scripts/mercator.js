Conv = (() => {
  return  Conv = ({
    r_major: 6378137.0,//Equatorial Radius, WGS84
    r_minor: 6356752.314245179,//defined as constant
    f: 298.257223563,//1/f=(a-b)/a , a=r_major, b=r_minor
    deg2rad: function (d) {
      let r = d * (Math.PI / 180.0);
      return r;
    },
    rad2deg: function (r) {
      let d = r / (Math.PI / 180.0);
      return d;
    },
    ll2m: function (lon, lat) //lat lon to mercator
    {
      //lat, lon in rad
      let x = this.r_major * this.deg2rad(lon);

      if (lat > 89.5) lat = 89.5;
      if (lat < -89.5) lat = -89.5;


      let temp = this.r_minor / this.r_major;
      let es = 1.0 - (temp * temp);
      let eccent = Math.sqrt(es);

      const phi = this.deg2rad(lat);

      const sinphi = Math.sin(phi);

      let con = eccent * sinphi;
      let com = .5 * eccent;
      let con2 = Math.pow((1.0 - con) / (1.0 + con), com);
      let ts = Math.tan(.5 * (Math.PI * 0.5 - phi)) / con2;
      let y = 0 - this.r_major * Math.log(ts);
      let ret = {'x': x, 'y': y};
      return ret;
    },
    m2ll: function (x, y) //mercator to lat lon
    {
      let lon = this.rad2deg((x / this.r_major));

      const temp = this.r_minor / this.r_major;
      const e = Math.sqrt(1.0 - (temp * temp));
      let lat = this.rad2deg(this.pj_phi2(Math.exp(0 - (y / this.r_major)), e));

      let ret = {'lon': lon, 'lat': lat};
      return ret;
    },
    pj_phi2: function (ts, e) {
      const N_ITER = 15;
      const HALFPI = Math.PI / 2;


      const TOL = 0.0000000001;
      var eccnth, Phi, con, dphi;
      let i;
      var eccnth = .5 * e;
      Phi = HALFPI - 2. * Math.atan(ts);
      i = N_ITER;
      do {
        con = e * Math.sin(Phi);
        dphi = HALFPI - 2. * Math.atan(ts * Math.pow((1. - con) / (1. + con), eccnth)) - Phi;
        Phi += dphi;

      }
      while (Math.abs(dphi) > TOL && --i);
      return Phi;
    }
  });
})();


//usage
var mercator=Conv.ll2m(30.466704, 59.769400);//output mercator.x, mercator.y
// var latlon=Conv.m2ll(5299424.36041, 1085840.05328);//output latlon.lat, latlon.lon
//
console.log("mercator", mercator);
// console.log("latlon", latlon);

