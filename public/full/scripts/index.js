const MAP_NEAR_EVROPA_AVTO = {
  lat: 59.769400,
  long: 30.466704,
  zoom: 16,
  id: 'mapNearEvropaAvto'
};

let panoramaCoordsXY = [];

const PSV_SETTINGS = {
  id: "viewer",
  img: "uvspb2.jpg"
};

// GeoJSON (https://leafletjs.com/examples/geojson/) (https://leafletjs.com/reference-1.5.0.html#polygon)
const POLYGON_COORDS = [
  [59.769248, 30.469980],
  [59.770109, 30.471665],
  [59.769979, 30.471901],
  [59.769865, 30.471922],
  [59.768556, 30.471536],
  [59.768491, 30.471364]
];

let polygonCoordsXY = [];

const MAPBOX_TOKEN = 'pk.eyJ1IjoiaXVydmluIiwiYSI6ImNqdmNleHYxYTA4Mmg0NHA4c2poZHY4Nm0ifQ.Xw-ZUYpgveUIjKnEelmf9w';


function addMap(params) {
  let mymap = L.map(params.id).setView([params.lat, params.long], params.zoom);

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    maxZoom: params.zoom,
    id: 'mapbox.streets',
    accessToken: MAPBOX_TOKEN
  }).addTo(mymap);

  return mymap;
}

function addPSV(params) {
  return new PhotoSphereViewer({
    panorama: `/full/images/${params.img}`,
    container: params.id,
    usexmpdata: false
  });
}

function addPolygonToMap(map, polygonCoords) {
  let stylePolygon = {color: 'red'};
  L.polygon(polygonCoords, stylePolygon).addTo(map);

  return map;
}

function addMarkerToMap(map, markerCoords) {
  L.marker(markerCoords).addTo(map);
  return map;
}

function covertLatLongToXY (latLongCoords) {
  let XY = [];

  latLongCoords.forEach((point) => {
    let xy = Conv.ll2m(point[1], point[0]);
    XY.push([xy.y, xy.x]);
  });

  return XY;
}

let map = addMap(MAP_NEAR_EVROPA_AVTO);
map = addPolygonToMap(map, POLYGON_COORDS);
map = addMarkerToMap(map, [MAP_NEAR_EVROPA_AVTO.lat, MAP_NEAR_EVROPA_AVTO.long]);

let PSV = addPSV(PSV_SETTINGS);

console.log('map', map);
console.log('PSV', PSV);

polygonCoordsXY = covertLatLongToXY(POLYGON_COORDS);
console.log('polygonCoordsXY', polygonCoordsXY);

panoramaCoordsXY = covertLatLongToXY([[MAP_NEAR_EVROPA_AVTO.lat, MAP_NEAR_EVROPA_AVTO.long]]);
console.log("panoramaCoordsXY", panoramaCoordsXY);