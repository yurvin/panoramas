const MAP_DVORCOVAYA =  {
  lat: 59.938960,
  long: 30.316385,
  zoom: 19,
  id: 'mapDvorcovaya'
};

const MAP_NEAR_EVROPA_AVTO = {
  lat: 59.768932,
  long: 30.465947,
  zoom: 18,
  id: 'mapNearEvropaAvto'
};

const MAPBOX_TOKEN = 'pk.eyJ1IjoiaXVydmluIiwiYSI6ImNqdmNleHYxYTA4Mmg0NHA4c2poZHY4Nm0ifQ.Xw-ZUYpgveUIjKnEelmf9w';

function addMap(params) {
  let mymap = L.map(params.id).setView([params.lat, params.long], params.zoom);

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: params.zoom,
    id: 'mapbox.streets',
    accessToken: MAPBOX_TOKEN
  }).addTo(mymap);
}

addMap(MAP_DVORCOVAYA);
addMap(MAP_NEAR_EVROPA_AVTO);